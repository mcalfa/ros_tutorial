ROS-Package "my_robot_tutorials"
ROS-Node "my_first_python_node" (rosrun my_robot_tutorials my_first_node.py)
ROS-Node "my_first_cpp_node" (rosrun my_robot_tutorials node_cpp)

ROS commands:
roscore
rosrun
rosnode list
rosnode info
rosnode ping
rosnode kill


ROS Graph:
rosrun rqt_graph rqt_graph


Example:
rosrun turtlesim turtlesim_node
rosrun turtlesim turtle_teleop_key

